package main

import "testing"

func TestIndex(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			name: "case 1 success",
			want: "hello world",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Index(); got != tt.want {
				t.Errorf("Index() = %v, want %v", got, tt.want)
			}
		})
	}
}
