package main

import (
	"fmt"
	"math"
	"sort"
	"strconv"
	"time"

	"github.com/montanaflynn/stats"
)

func Index() string {

	return "server is good"

}

func (a *App) Search(p SearchParam) (msg string, resp SearchResponse) {

	t1 := time.Now()

	items, productCount := a.paginateProductCrawl(p)

	t2 := time.Now()

	resp.Products = a.poolGetProductDetail(items)

	t3 := time.Now()

	resp.Analytics = analyseProduct(resp.Products)
	resp.Analytics.AllProductCount = int(productCount)

	resp.Perfomance.SearchAPI = handleTime(t2.Sub(t1))
	resp.Perfomance.ProductAPI = handleTime(t3.Sub(t2))
	resp.Perfomance.AllProcess = handleTime(t3.Sub(t1))

	msg = fmt.Sprintf("success getting product with detail (%d)", len(items))

	return
}

func (a *App) paginateProductCrawl(sp SearchParam) (items []Item, productCount float64) {

	pages := int(math.Ceil(float64((sp.WantedItem / DEFAULT_PAGE_SIZE))))
	totals := []float64{}

	for i := 0; i < pages; i++ {

		p := Param{}
		p.Data = sp
		p.Offset = DEFAULT_PAGE_SIZE * i
		p.Page = i

		resp := a.doProductCrawl(p)

		totals = append(totals, float64(resp.TotalCount))
		items = append(items, resp.Items...)
	}

	if len(items) > int(sp.WantedItem) {
		items = items[0:int(sp.WantedItem)]
	}

	productCount, _ = stats.Max(totals)

	return
}

func (a *App) poolGetProductDetail(items []Item) (details []Detail) {

	numJobs := len(items)
	jobs := make(chan Item, numJobs)
	results := make(chan Detail, numJobs)

	for w := 1; w <= DEFAULT_WORKER; w++ {
		go a.workerGetProductDetail(w, jobs, results)
	}

	for _, j := range items {
		jobs <- j
	}

	for a := 1; a <= numJobs; a++ {
		details = append(details, <-results)
	}

	close(jobs)

	return
}

func (a *App) workerGetProductDetail(id int, jobs <-chan Item, results chan<- Detail) {

	for j := range jobs {
		res := a.doDetailCrawl(j)
		res = handlePrice(res)
		results <- res
	}

}

func handlePrice(old Detail) (new Detail) {

	new = old

	new.Item.Price = old.Item.Price / 100000
	new.Item.PriceBeforeDiscount = old.Item.PriceBeforeDiscount / 100000
	new.Item.PriceMinBeforeDiscount = old.Item.PriceMinBeforeDiscount / 100000
	new.Item.PriceMax = old.Item.PriceMax / 100000
	new.Item.PriceMin = old.Item.PriceMin / 100000

	return
}

func handleTime(t time.Duration) (f string) {

	d := time.Millisecond
	f = (t.Truncate(d)).String()

	return
}

func findMax(v1, v2 int) int {
	if v1 < v2 {
		return v1
	}

	return v2
}

func ts(v int) string {
	return strconv.Itoa(v)
}

func analyseProduct(details []Detail) (analytics Analytics) {

	prices := []float64{}
	shops := make(map[int]int)

	for _, d := range details {
		prices = append(prices, d.Item.Price)
		shops[d.Item.Shopid]++
	}

	sort.Sort(sort.Float64Slice(prices))

	analytics.ShopCount = len(shops)

	priceMean, _ := stats.Mean(prices)
	priceMedian, _ := stats.Median(prices)
	priceMax, _ := stats.Max(prices)
	priceMin, _ := stats.Min(prices)
	priceStdev, _ := stats.StdDevP(prices)

	analytics.Prices = prices
	analytics.PriceMean = int(priceMean)
	analytics.PriceMedian = int(priceMedian)
	analytics.PriceMax = int(priceMax)
	analytics.PriceMin = int(priceMin)
	analytics.PriceStdev = math.Round((priceStdev/priceMean)*10000) / 100

	labels := []int{}

	priceVal := []int{}
	salesVal := []int{}
	likeVal := []int{}

	var i float64

	labels = append(labels, 0)
	priceVal = append(priceVal, 0)
	salesVal = append(salesVal, 0)
	likeVal = append(likeVal, 0)

	for i = 0; i < 10; i++ {

		mi, _ := stats.Percentile(prices, i*10)
		ma, _ := stats.Percentile(prices, (i+1)*10)

		pmin := int(mi)
		pmax := int(ma)

		labels = append(labels, int(pmax))
		pCount := 0
		sCount := 0
		lCount := 0

		for _, p := range details {

			if int(p.Item.Price) >= pmin && int(p.Item.Price) <= pmax && i == 0 {
				pCount++
				sCount += findMax(p.Item.Sold, p.Item.HistoricalSold)
				lCount += p.Item.LikedCount
			} else if int(p.Item.Price) > pmin && int(p.Item.Price) <= pmax {
				pCount++
				sCount += findMax(p.Item.Sold, p.Item.HistoricalSold)
				lCount += p.Item.LikedCount
			}
		}

		priceVal = append(priceVal, pCount)
		salesVal = append(salesVal, sCount)
		likeVal = append(likeVal, lCount)
	}

	analytics.PriceChart.Line.Labels = labels
	analytics.PriceChart.Line.ProductCount = priceVal
	analytics.PriceChart.Line.SalesCount = salesVal
	analytics.PriceChart.Line.LikesCount = likeVal

	return
}
