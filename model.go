package main

type Response struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type SearchResponse struct {
	Analytics  Analytics          `json:"analytics"`
	Products   []Detail           `json:"products"`
	Perfomance CrawlingPerfomance `json:"perfomance"`
}

type CrawlingPerfomance struct {
	SearchAPI  string `json:"search_api"`
	ProductAPI string `json:"product_api"`
	AllProcess string `json:"all_process"`
}

type Param struct {
	Data   SearchParam
	Offset int
	Page   int
}

type SearchParam struct {
	Keyword    string
	MinPrice   string
	WantedItem float64
}

type ShpRes struct {
	TotalCount      int           `json:"total_count"`
	Error           interface{}   `json:"error"`
	Items           []Item        `json:"items"`
	ReservedKeyword string        `json:"reserved_keyword"`
	HintKeywords    []interface{} `json:"hint_keywords"`
}

type Item struct {
	Itemid int64  `json:"itemid"`
	Shopid int    `json:"shopid"`
	Catid  int    `json:"catid"`
	Name   string `json:"name"`
}

type Analytics struct {
	PriceMean       int        `json:"price_mean"`
	PriceMedian     int        `json:"price_median"`
	PriceMax        int        `json:"price_max"`
	PriceMin        int        `json:"price_min"`
	PriceStdev      float64    `json:"price_stdev"`
	Prices          []float64  `json:"prices"`
	AllProductCount int        `json:"product_count"`
	ShopCount       int        `json:"shop_count"`
	PriceChart      PriceChart `json:"price_chart"`
}

type PriceChart struct {
	Line Line `json:"line"`
}

type Line struct {
	Labels       []int `json:"labels"`
	ProductCount []int `json:"product_count"`
	SalesCount   []int `json:"sales_count"`
	LikesCount   []int `json:"likes_count"`
}

type Detail struct {
	Item struct {
		Itemid int64  `json:"itemid"`
		Name   string `json:"name"`
		Brand  string `json:"brand"`

		ItemStatus       string `json:"item_status"`
		IsSlashPriceItem bool   `json:"is_slash_price_item"`
		Condition        int    `json:"condition"`
		Categories       []struct {
			DisplayName string `json:"display_name"`
			Catid       int    `json:"catid"`
		} `json:"categories"`
		Images      []string `json:"images"`
		Description string   `json:"description"`

		NormalStock int `json:"normal_stock"`
		Stock       int `json:"stock"`

		Price                  float64 `json:"price"`
		PriceMinBeforeDiscount float64 `json:"price_min_before_discount"`
		PriceBeforeDiscount    float64 `json:"price_before_discount"`
		PriceMax               float64 `json:"price_max"`
		PriceMin               float64 `json:"price_min"`
		Discount               string  `json:"discount"`
		RawDiscount            int     `json:"raw_discount"`

		HistoricalSold int `json:"historical_sold"`
		Sold           int `json:"sold"`
		ItemRating     struct {
			RatingStar        float64 `json:"rating_star"`
			RatingCount       []int   `json:"rating_count"`
			RcountWithImage   int     `json:"rcount_with_image"`
			RcountWithContext int     `json:"rcount_with_context"`
		} `json:"item_rating"`
		LikedCount int         `json:"liked_count"`
		CmtCount   int         `json:"cmt_count"`
		ViewCount  interface{} `json:"view_count"`

		Shopid         int    `json:"shopid"`
		IsOfficialShop bool   `json:"is_official_shop"`
		ShopLocation   string `json:"shop_location"`
	} `json:"item"`
	Version  string      `json:"version"`
	Data     interface{} `json:"data"`
	ErrorMsg interface{} `json:"error_msg"`
	Error    interface{} `json:"error"`
}
