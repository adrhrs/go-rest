package main

const (
	APP_PORT   = ":9898"
	STATIC_DIR = "/static/"

	DEFAULT_PAGE_SIZE = 50
	DEFAULT_WORKER    = 32
)
