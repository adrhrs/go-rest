package main

import (
	"encoding/json"
	"net/http"
	"strconv"
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	msg := Index()
	res.Data = msg

	return

}

func (a *App) SearchHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	keyword := r.FormValue("keyword")
	if keyword == "" {
		res.Message = "keyword is required"
		return
	}

	minPrice := r.FormValue("min_price")
	if minPrice == "" {
		res.Message = "min_price is required"
		return
	}

	wantedItem, err := strconv.ParseFloat(r.FormValue("wanted_item"), 64)
	if err != nil {
		res.Message = err.Error()
		return
	}

	param := SearchParam{
		Keyword:    keyword,
		MinPrice:   minPrice,
		WantedItem: wantedItem,
	}

	msg, items := a.Search(param)
	res.Message = msg
	res.Data = items

	return

}
