package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

type App struct {
	Router *mux.Router
	Client *http.Client
}

func (a *App) initialize() {

	t0 := time.Now()

	a.Router = a.loadRouter()
	a.Client = &http.Client{}

	t1 := time.Now()

	// Where ORIGIN_ALLOWED is like `scheme://dns[:port]`, or `*` (insecure)
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	log.Printf("running on %s, init on %s ", APP_PORT, t1.Sub(t0))
	log.Fatal(http.ListenAndServe(APP_PORT, handlers.CORS(headersOk, methodsOk)(a.Router)))

}

func main() {

	a := App{}
	a.initialize()

}
