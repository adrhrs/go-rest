package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func (a *App) loadRouter() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/api/health", IndexHandler)
	r.HandleFunc("/api/shp/search", a.SearchHandler)

	r.Use(loggingMiddleware)

	return r
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t1 := time.Now()
		next.ServeHTTP(w, r)
		t2 := time.Now()
		log.Println(r.Method, t2.Sub(t1))
	})
}
